(function ($, Drupal) {

    'use strict';

    /**
     * Initialize input masks.
     *
     * @type {Drupal~behavior}
     */
    Drupal.behaviors.FieldMask = {
        attach: function (context) {

            var inputmask = $(context).find('select#fieldmask-input-mask');
            var inputmaskother = $(context).find('input#fieldmask-input-mask-other');

            inputmask.change(function () {
                var FieldmaskSelectValue = $(this).val();
                if (FieldmaskSelectValue == 'other') {
                    inputmaskother.parent().show();
                } else {
                    inputmaskother.val(" ");
                    inputmaskother.parent().hide();
                }
            });

            if (inputmaskother.val() == " ") {
                inputmaskother.parent().hide();
            }
        }
    };

})(jQuery, Drupal);
