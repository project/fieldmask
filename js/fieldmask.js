/**
 * @file
 * JavaScript behaviors for jquery.inputmask integration.
 */

(function ($, Drupal) {

    'use strict';

    // Revert: Set currency prefix to empty by default #2066.
    // @see https://github.com/RobinHerbots/Inputmask/issues/2066
    if (window.Inputmask) {
        window.Inputmask.extendAliases({
            currency: {
                prefix: '$ ',
                groupSeparator: ',',
                alias: 'numeric',
                placeholder: '0',
                autoGroup: TRUE,
                digits: 2,
                digitsOptional: FALSE,
                clearMaskOnLostFocus: FALSE
            }
        });
    }

    /**
     * Initialize input masks.
     *
     * @type {Drupal~behavior}
     */
    Drupal.behaviors.FieldMask = {
        attach: function (context) {
            if (!$.fn.inputmask) {
                return;
            }

            $(context).find('input.js-field-mask-input').once('field-mask-input').inputmask();

        }
    };

})(jQuery, Drupal);
