# FieldMask

Field mask module able to modify simple text fields with an Input mask for the
admin interface. By using, the module can restrict node editor to fellow mask
rule. The module filter is based on the input mask library.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/fieldmask).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/fieldmask).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing
Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


## Maintainers

Supporting by:

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
