<?php

namespace Drupal\fieldmask\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_mask_widget' widget.
 *
 * @FieldWidget(
 *   id = "field_mask_widget",
 *   module = "fieldmask",
 *   label = @Translation("Field mask widget"),
 *   field_types = {
 *     "field_mask"
 *   }
 * )
 */
class FieldMaskWidget extends WidgetBase {
  /**
   * Summary of fieldmask.
   *
   * @var mixed
   *   The mixed.
   */
  private $fieldmask;

  /**
   * Summary of defaultSettings.
   *
   * @return array
   *   It will return array.
   */
  public static function defaultSettings() {
    return [
      'size' => 60,
      'placeholder' => '',
      'input_mask' => '',
      'input_mask_other' => '',
    ] + parent::defaultSettings();
  }

  /**
   * Summary of settingsForm.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state.
   *
   * @return array
   *   It will return array.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['#attached']['library'][] = 'fieldmask/fieldmask.inputmask.admin';

    $elements['size'] = [
      '#type' => 'number',
      '#title' => $this->t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
    ];
    $elements['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    // Input mask.
    $elements['input_mask'] = [
      '#type' => 'select',
      '#title' => $this->t('Input masks'),
      '#description' => $this->t('An <a href=":href">inputmask</a> helps the user with the element by ensuring a predefined format.', [':href' => 'https://github.com/RobinHerbots/jquery.inputmask']),
      '#empty_option' => $this->t('- None -'),
      '#default_value' => $this->getSetting('input_mask'),
      '#options' => $this->getInputMaskOptions(),
      '#attributes' => [
        'id' => 'fieldmask-input-mask',
      ],
    ];

    $elements['input_mask_other'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom…'),
      '#placeholder' => $this->t('Enter input mask…'),
      '#description' => $this->t('(9 = numeric; a = alphabetical; * = alphanumeric)'),
      '#default_value' => $this->getSetting('input_mask_other'),
      '#attributes' => ['id' => 'fieldmask-input-mask-other'],
    ];

    return $elements;
  }

  /**
   * Summary of settingsSummary.
   *
   * @return array
   *   It will return array.
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Textfield size: @size', ['@size' => $this->getSetting('size')]);
    if (!empty($this->getSetting('placeholder'))) {
      $summary[] = $this->t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder')]);
    }
    if (!empty($this->getSetting('input_mask'))) {
      $summary[] = $this->t('Input Mask: @input_mask', ['@input_mask' => $this->getSetting('input_mask')]);
    }
    if ($this->getSetting('input_mask_other') != " ") {
      $summary[] = $this->t('Input Mask Other: @input_mask_other', ['@input_mask_other' => $this->getSetting('input_mask_other')]);
    }

    return $summary;
  }

  /**
   * Summary of formElement.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The items.
   * @param mixed $delta
   *   The delta.
   * @param array $element
   *   The element.
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state.
   *
   * @return array
   *   It will return array.
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $inputmask = $this->getSetting('input_mask');
    $inputmaskother = $this->getSetting('input_mask_other');

    if (!empty($inputmask)) {
      $element['#attached']['library'][] = 'fieldmask/fieldmask.inputmask';
    }

    if ((!empty($inputmask) && $inputmaskother == " ")) {

      $this->fieldmask = [
        'class' => ['js-field-mask-input'],
        'data-inputmask' => $this->getSetting('input_mask'),
      ];

    }
    elseif ((!empty($inputmask) && $inputmaskother != " ")) {

      $this->fieldmask = [
        'class' => ['js-field-mask-input'],
        'data-inputmask' => "'mask':'" . $inputmaskother . "'",
      ];

    }

    if ($inputmask == "'alias': 'datetime'") {

      $element['value'] = $element + [
        '#type' => 'date',
        '#default_value' => $items[$delta]->value ?? NULL,
        '#date_date_format' => 'dd/mm/yyyy',
        '#attributes' => $this->fieldmask,
      ];

    }
    else {

      $element['value'] = $element + [
        '#type' => 'textfield',
        '#default_value' => $items[$delta]->value ?? NULL,
        '#size' => $this->getSetting('size'),
        '#placeholder' => $this->getSetting('placeholder'),
        '#maxlength' => $this->getFieldSetting('max_length'),
        '#attributes' => $this->fieldmask,
      ];
    }

    return $element;
  }

  /**
   * Get input masks as select menu options.
   *
   * @return array
   *   An associative array of options.
   */
  public function getInputMaskOptions() {
    $input_masks = $this->getInputMasks();
    $options = [];
    foreach ($input_masks as $input_mask => $settings) {
      $options[$input_mask] = $settings['title'] . (!empty($settings['example']) ? ' - ' . $settings['example'] : '');
    }
    return $options;
  }

  /**
   * Get input masks.
   *
   * @return array
   *   An associative array keyed my input mask contain input mask title,
   *   example, and patterh.
   */
  public function getInputMasks() {
    $input_masks = [
      "'alias': 'currency'" => [
        'title' => $this->t('Currency'),
        'example' => '$ 9.99',
        'pattern' => '^\$ [0-9]{1,3}(,[0-9]{3})*.\d\d$',
      ],
      "'alias': 'datetime'" => [
        'title' => $this->t('Date'),
        'example' => '2007-06-09\'T\'17:46:21',
      ],
      "'alias': 'decimal'" => [
        'title' => $this->t('Decimal'),
        'example' => '1.234',
        'pattern' => '^\d+(\.\d+)?$',
      ],
      "'alias': 'email'" => [
        'title' => $this->t('Email'),
        'example' => 'example@example.com',
      ],
      "'alias': 'ip'" => [
        'title' => $this->t('IP address'),
        'example' => '255.255.255.255',
        'pattern' => '^\d\d\d\.\d\d\d\.\d\d\d\.\d\d\d$',
      ],
      "'mask':'[9-]AAA-999'" => [
        'title' => $this->t('License plate'),
        'example' => '[9-]AAA-999',
      ],
      "'alias': 'mac'" => [
        'title' => $this->t('MAC address'),
        'example' => '99-99-99-99-99-99',
        'pattern' => '^\d\d-\d\d-\d\d-\d\d-\d\d-\d\d$',
      ],
      "'alias': 'percentage'" => [
        'title' => $this->t('Percentage'),
        'example' => '99 %',
        'pattern' => '^\d+ %$',
      ],
      "'mask':'(999) 999-9999'" => [
        'title' => $this->t('Phone'),
        'example' => '(999) 999-9999',
        'pattern' => '^\(\d\d\d\) \d\d\d-\d\d\d\d$',
      ],
      "'mask':'999-99-9999'" => [
        'title' => $this->t('Social Security Number (SSN)'),
        'example' => '999-99-9999',
        'pattern' => '^\d\d\d-\d\d-\d\d\d\d$',
      ],
      "'alias': 'vin'" => [
        'title' => $this->t('Vehicle identification number (VIN)'),
        'example' => 'JA3AY11A82U020534',
      ],
      "'mask':'99999[-9999]'" => [
        'title' => $this->t('ZIP Code'),
        'example' => '99999[-9999]',
        'pattern' => '^\d\d\d\d\d(-\d\d\d\d)?$',
      ],
      "'casing': 'upper'" => [
        'title' => $this->t('Uppercase'),
        'example' => 'UPPERCASE',
      ],
      "'casing': 'lower'" => [
        'title' => $this->t('Lowercase'),
        'example' => 'lowercase',
      ],
      "other" => [
        'title' => $this->t('Other...'),
      ],
    ];

    return $input_masks;
  }

}
